#include <iostream>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>

int main() {
    // 打开串口
    const char *serialPort = "/dev/ttyS0"; // 串口号
    int fd = open(serialPort, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) {
        std::cerr << "打开串口失败" << std::endl;
        return 1;
    }

    // 设置串口参数
    struct termios tty;
    if (tcgetattr(fd, &tty) != 0) {
        std::cerr << "获取串口属性失败" << std::endl;
        close(fd);
        return 1;
    }

    // 设置波特率
   	cfsetispeed(&tty, B9600);
   	cfsetospeed(&tty, B9600);

    // 设置数据位
   	tty.c_lflag &= ~CSIZE;
    tty.c_lflag |= CS8;

    // 设置停止位
    tty.c_cflag &= ~CSTOPB;

    // 设置奇偶校验
    tty.c_cflag &= ~PARENB;

    // 清除标志
    tty.c_cflag |= (CLOCAL | CREAD);

    // 写入串口
    const char *data = "AT";
    write(fd, data, strlen(data));

    // 使用select等待串口输入
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(fd, &readfds);

    int activity = select(1, &readfds, nullptr, nullptr, nullptr);
    if (activity == -1) {
        std::cerr << "select函数调用失败" << std::endl;
        close(fd);
        return 1;
    }

    if (FD_ISSET(fd, &readfds)) {
        char buffer[20];
        read(fd, buffer, sizeof(buffer) - 1);
        buffer[sizeof(buffer) - 1] = '\0';
        std::cout << "从串口读取数据： " << buffer << std::endl;
    }

    // 关闭串口
    close(fd);

    return 0;
}
