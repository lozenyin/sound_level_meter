
# 声级计程序

## 工程简介

声级计程序主要是针对声仪的声级计使用。包括获取、设置和校准等操作。主要是依赖linux串口进行数据读写操作。原先使用的是Python库编写的，但是开发人员反馈接口获取数据有很大概率获取不到。需要研究研究这种情况。

## 工程代码结构

```bash

```

## 编译

进入编译目录
```bash
cd build
```

执行编译

```bash
rm -rf * && cmake .. && make
```

## 部署使用


## 参考

> 需要安装Markdown Preview Mermaid Support插件

```mermaid
graph LR;
    串口类-->modbus类;
    串口类-->AT指令类;
    串口类-->特殊协议类;
    特殊协议类-->自研驱动板;
    modbus类-->声仪声强计;
    modbus类-->ZS_12;
    AT指令类-->4G模块;
    4G模块-->EC20;


```
1. [Linux系统串口接收数据编程-duhui75-CSDN](https://blog.csdn.net/duhui75/article/details/122578147)