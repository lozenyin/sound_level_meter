#include "SerialModbusDev.h"


ZS_12::ZS_12(std::string path,std::string baud_rate):SerialModbus(path,baud_rate)
{
}

ZS_12::~ZS_12()
{
}

int ZS_12::get_db(){
    
    //发送指令:功能码、起始地址、数据个数
    send_command(dev_addr,0x03,{0x00,0x00},{0x00,0x01});

    //接收返回的数据
    std::vector<unsigned char> redata = receive(7);

    //解析数据
    // printf_command(redata,"redata:");
    if(redata.size() > 0){
        uint16_t db = (redata[3]<<8)+redata[4];
        // printf(">>> db:%d \n\r",db);
        return db;
    }else{
        return -1;
    }
}

unsigned chat get_addr(){
    send_command();

}