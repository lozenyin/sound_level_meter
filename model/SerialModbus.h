#include "SerialPort.h"

#include <vector>

class SerialModbus:public SerialPort 
{
private:
    /* data */

public:

    unsigned char dev_addr;//从站地址

    // SerialModbus(std::string path,int baud_rate);
    SerialModbus(std::string path,std::string baud_rate);
    ~SerialModbus();

    int send(std::vector<unsigned char>& command);//发送指令
    int send_command(unsigned char devaddr,unsigned char code,std::vector<unsigned char> addr,std::vector<unsigned char> len);
    std::vector<unsigned char> receive(int len);//接收信息

    int CRC16_modbus(std::vector<unsigned char>& com);
    void compute_CRC16(std::vector<unsigned char>& com);//CRC16校验码计算
    bool check_CRC16(std::vector<unsigned char>& com);
    void printf_command(std::vector<unsigned char>& com,std::string tap);

    //
    void add_device_address();//向指令添加从机地址

    //获取声强值
    int get_db();

};



