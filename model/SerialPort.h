#include <iostream>
#include <string>

#include <termios.h> // POSIX串口通信API
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>

#define flag 1
#define noflag 0

class SerialPort
{
private:
    /* data */
    std::string device;//涉笔路径
    int baudRate;//波特率
    int date_bits;//数据位

    struct termios options;//串口配置变量
    int serial_port_fd;//串口文件句柄

public:
    SerialPort(std::string path,std::string baud_rate);
    ~SerialPort();

    bool open_dev();
    bool config_dev();
    bool config_dev(int databits,int stopbits,int parity);
    bool check_dev();
    void close_dev();

    int readData(char *buffer, size_t len);         //接收数据
    int readData_one(unsigned char *buffer);
    int writeData(const char *buffer, size_t len);  //发送数据

    std::string decimalToBinary(unsigned int decimal);
    // int check_dev_is_data();

};








