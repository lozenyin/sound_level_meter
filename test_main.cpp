#include <iostream>

// #include "SerialPort.h"
// #include "SerialModbus.h"
#include "SerialModbusDev.h"

//测试使用

int main(){

    std::cout << "ZS_12 声级计测试程序" << std::endl;

    ZS_12 zs_12 = ZS_12("/dev/ttyS0","9600");
    // ZS_12 *zs_12 =new ZS_12("/dev/ttyS0","9600");

    while (1)
    {
        float db = zs_12.get_db();
        if (db > 0)
        {
            std::cout << "分贝值："<< db/10 << std::endl;
        } 
        sleep(0.1);
    }

    return 0;
}


