try:
    import os,sys,time
    import serial
    import binascii

except ImportError:
    print(">>> lib not found !!!")
    print(">>> Please pip3 install pyserial !")
    exit(1)

# 初始化函数
def tty_init(dev,bsp):
    # 初始化设备，并打开
    ser=serial.Serial(dev,bsp,
                      bytesize=serial.EIGHTBITS,    # 数据位：8位
                      parity=serial.PARITY_NONE,    # 校验位：无
                      stopbits=serial.STOPBITS_ONE, # 停止位：1位
                      timeout=None)                 # 读超时时间
    # 判断是否打开成功
    if ser:
        print("打开串口成功,串口详情参数：",ser)
    else:
        print("串口打开失败！")
    return ser #返回设备信息及句柄

# 获取当前声强值
def get_voice_strength():
    result=ser.write(bytes.fromhex('01 03 00 00 00 01 84 0A'))

    time.sleep(0.1) #g
    count=ser.inWaiting()

    if count>0:
        data=ser.read(count)
        if data!=b'':
            # 将接受的16进制数据格式如b'h\x12\x90xV5\x12h\x91\n4737E\xc3\xab\x89hE\xe0\x16'
            #                      转换成b'6812907856351268910a3437333745c3ab896845e016'
            #                      通过[]去除前后的b'',得到我们真正想要的数据 
            receive=str(binascii.b2a_hex(data))[2:-1]
            print("receive",receive)

            return int(receive[6:10],16)

if __name__ == "__main__":

    ser = tty_init("/dev/ttyS0",9600)

    print(get_voice_strength())
