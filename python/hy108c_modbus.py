# 湖南声仪的声级计，使用的是modebus协议的
try:
    import os,sys,time
    import serial
    import binascii

except ImportError:
    print(">>> lib not found !!!")
    print(">>> Please pip3 install pyserial !")
    exit(1)

class HY108C:
    
    def __init__(self,dev,bsp):
        self.init(dev,bsp)
        
    def __del__(self):
        self.ser.close() #关闭串口
        
    # 初始化通信接口
    def init(self,dev,bsp):
        # 打开串口
        self.ser=serial.Serial(dev,bsp,
                            bytesize=serial.EIGHTBITS,    # 数据位：8位
                            parity=serial.PARITY_NONE,    # 校验位：无
                            stopbits=serial.STOPBITS_ONE, # 停止位：1位
                            timeout=None)                 # 读超时时间
        # 判断是否打开成功
        if self.ser:
            print("打开串口成功,串口详情参数：",self.ser)
        else:
            print("串口打开失败！")
        # return ser #返回设备信息及句柄
    
    # 复位最大值
    def reset_db_max(self):
        result=self.ser.write(bytes.fromhex('01 06 00 04 00 01 09 CB'))
    
    # 查询当前的时间计权模式
    def get_time_weighting(self):
        result=self.ser.write(bytes.fromhex('01 04 00 03 00 01 C1 CA'))
        
        time.sleep(0.1)
        count=self.ser.inWaiting()
        print(">>> count:%d"%count)
        if count>0:
            data=self.ser.read(count)
            if data!=b'':
                # 将接受的16进制数据格式如b'h\x12\x90xV5\x12h\x91\n4737E\xc3\xab\x89hE\xe0\x16'
                #                      转换成b'6812907856351268910a3437333745c3ab896845e016'
                #                      通过[]去除前后的b'',得到我们真正想要的数据 
                receive=str(binascii.b2a_hex(data))[2:-1]
                if receive[0:6] == "010402":
                    if int(receive[6:10],16) == 0:
                        return 'F'
                    elif int(receive[6:10],16) == 1:
                        return 'S'
                    else:
                        return 99
                else:
                    return 99
            else:
                print("no data!!")
                return 99
        
    # 设置时间计权方式,F:125ms，S：1s
    def set_time_weighting(self,mode):
        if mode == 'S':
            result=self.ser.write(bytes.fromhex('01 06 00 03 00 01 B8 0A'))
        elif mode == 'F':
            result=self.ser.write(bytes.fromhex('01 06 00 03 00 00 79 CA'))
        else:
            return 99
        
    # 获取上次复位到现在的最大值
    def get_db_max(self):
        result=self.ser.write(bytes.fromhex('01 03 00 01 00 01 D5 CA'))

        time.sleep(0.1)
        count=self.ser.inWaiting()
        print(">>> count:%d"%count)
        if count>0:
            data=self.ser.read(count)
            if data!=b'':
                receive=str(binascii.b2a_hex(data))[2:-1]
                if receive[0:6] == "010302":
                    return int(receive[6:10],16)
                else:
                    return None
    
    # 获取瞬时值
    def get_db(self):
        result=self.ser.write(bytes.fromhex('01 03 00 00 00 01 84 0A'))

        time.sleep(0.1)
        count=self.ser.inWaiting()
        print(">>> count:%d"%count)
        if count>0:
            data=self.ser.read(count)
            if data!=b'':
                receive=str(binascii.b2a_hex(data))[2:-1]
                if receive[0:6] == "010302":
                    return int(receive[6:10],16)
                else:
                    return None
        
    # 返回状态：0校准成功，1校准中，2声级太高，3声级太低，4声级不稳
    def get_calibration_state(self):
        result=self.ser.write(bytes.fromhex('01 04 00 01 00 01 60 0A'))
        time.sleep(0.1)
        count=self.ser.inWaiting()
        print(">>> count:%d"%count)
        if count>0:
            data=self.ser.read(count)
            if data!=b'':
                receive=str(binascii.b2a_hex(data))[2:-1]
                if receive[0:6] == "010302":
                    return int(receive[6:10],16)
                else:
                    return None
        
    # 校准操作
    # ps:这个操作实际上在本地直接操作设备会比较好
    def calibration(self):
        result=self.ser.write(bytes.fromhex('01 06 00 01 00 01 19 CA'))
        
        # 等待6秒以上
        time.sleep(7)
        
        # 读取校准状态
        ret = self.get_calibration_state()
        if ret==0:
            print("校准成功！")
        elif ret == 1:
            print("校准中……")
        elif ret == 2:
            print("声级太高！")
        elif ret == 3:
            print("声级太低！")
        elif ret == 4:
            print("声级不稳！")
        else:
            return 99
    # 获取设备型号
    def get_device_type(self):
        instruct=self.addr+' 03 00 03 00 05'
        crc_str=self.crc16_checksum(bytes.fromhex(instruct))
        instruct=instruct+' '+crc_str[2]+crc_str[3]+' '+crc_str[0]+crc_str[1]
        result=self.ser.write(bytes.fromhex(instruct))
        
        time.sleep(0.05)
        count=self.ser.inWaiting()
        print(">>> get_device_type count:%d"%count)
        if count>0:
            data=self.ser.read(count)
            if data!=b'':
                # receive=str(binascii.b2a_hex(data))[2:-1]
                return chr(data[3])+chr(data[4])+chr(data[5])+chr(data[6])+chr(data[7])+chr(data[8])
        
    # CRC16校验
    def crc16(self,data):
        crc = 0xFFFF
        for byte in data:
            crc ^= byte
            for _ in range(8):
                if crc & 0x0001:
                    crc >>= 1
                    crc ^= 0xA001
                else:
                    crc >>= 1
        return hex(crc)[2:].upper().zfill(4)

    def crc16_checksum(self,hex_string):
        crc = 0xFFFF
        for byte in hex_string:
            crc ^= byte
            for _ in range(8):
                if crc & 0x0001:
                    crc >>= 1
                    crc ^= 0xA001
                else:
                    crc >>= 1
        return hex(crc)[2:].zfill(4)

if __name__ == "__main__":
    print("湖南声仪HY108C声级计例程")
    
    # 实例化
    hy108c_dev = HY108C("/dev/ttyS0",9600)
    # 默认使用F时间计权模式
    hy108c_dev.set_time_weighting('F')
    
    # 相关操作
    # 获取瞬时值
    # print(hy108c_dev.get_db_max())

    while 1:
        print(hy108c_dev.get_db())
        time.sleep(1)
    # hy108c_dev.reset()
    
    # hy108c_dev.calibration()
    
