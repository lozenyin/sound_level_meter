try:
    import os,sys,time
    import serial
    import binascii
    import struct

except ImportError:
    print(">>> lib not found !!!")
    print(">>> Please pip3 install pyserial !")
    exit(1)


class HY108C:
    
    def __init__(self,dev,bsp):
        self.init(dev,bsp)
        
    def __del__(self):
        self.ser.close() #关闭串口
        
    # 初始化通信接口
    def init(self,dev,bsp):
        # 打开串口
        self.ser=serial.Serial(dev,bsp,
                            bytesize=serial.EIGHTBITS,    # 数据位：8位
                            parity=serial.PARITY_NONE,    # 校验位：无
                            stopbits=serial.STOPBITS_ONE, # 停止位：1位
                            timeout=None)                 # 读超时时间
        # 判断是否打开成功
        if self.ser:
            print("打开串口成功,串口详情参数：",self.ser)
        else:
            print("串口打开失败！")
        # return ser #返回设备信息及句柄
    
    
    
    # 获取瞬时值
    def get_db(self):
        result=self.ser.write(b'L')

        time.sleep(0.1)
        count=self.ser.inWaiting()
        print(">>> count:%d"%count)
        if count>0:
            data=self.ser.read(count)
            if data!=b'':
                return float(data[1:6])
        

if __name__ == "__main__":
    print("湖南声仪HY108C声级计例程(单一数据模式)")
    
    # 实例化
    hy108c_dev = HY108C("/dev/ttyS0",9600)
   
    print(hy108c_dev.get_db())


