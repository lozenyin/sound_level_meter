import os,sys,time
import struct
import serial
import binascii
import re

from FX_PWR_CTL import FX_PWR_CTL

if __name__ == "__main__":
    print("自研延时断电模块控制例程")
    
    # 实例化
    dev = FX_PWR_CTL("/dev/ttyS4",115200)
    
    num_list = []
    while True:
        print("\n请输入指令:")
        command = input()
        
        if command == "-v":
            print("固件版本：",dev.get_version())
        elif command == "V":
            print("当前电压：",dev.get_voltage())
        elif command == "OTA":
            
            all_files = os.listdir()
            print(all_files)
            filename = input("请输入文件名：")
            print("OTA开始升级~~~")
            dev.OTA_update(filename)
        elif command == "exit":
            break
        elif command == "time":
            print("延时断电时间：",dev.get_deley_time())
        elif command == "test":
            dev.test()
        elif command == "type":
            # 电源检测，1为电池，0为电源适配器
            if dev.get_powertype() == 1:
                print("电池")
            else:
                print("电源适配器")
        elif command == "poweroff":
            # 延时关机函数
            if dev.notify_shutdown()==True:
                print("已通知关机！")# 通知关机
            else:
                print("未通知到！！！")
        elif command == "init_set":
            # 设置初始值
            dev.set_delay_power_off_time(19000) # 关机延时时间
            dev.set_low_battery(12400)          # 低电压阀值
        elif command == "printf":
            # 打印全部状态
            print("固件版本：",dev.get_version())
            print("延时断电时间：",dev.get_delay_time())
            print("低电压阀值：",dev.get_low_batterye())
            print("当前电压：",dev.get_voltage())
            if dev.get_powertype() == 1:
                print("当前使用：电池")
            else:
                print("当前使用：电源适配器")
            
        else:
            print("无效的指令，请重新输入。")